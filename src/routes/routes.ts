/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Description:
 * Root enry level file forr bootstarting node js application
 */

import express from "express";
import {PostRespBusinessObjects}    from "../mapping/bussObj/Response";
import FinanceController            from "../mapping/FinancesConroller";


const router: any = express.Router();

/**
 * Route to fetch finances
 */
router.get('/finance/:id', (req:any, res: any) => {            
    if(req.query.type===undefined || (req.query.type !== 'cif' && req.query.type !== 'fid')){                    
        res.status(404);   
        res.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("9404","Invalid Request!"));        
    }else{                
        let finController:FinanceController = new FinanceController();
        if(req.query.type === 'cif'){
            finController.setCustomerID = req.params.id;
        }else{
            finController.setFinanceID = req.params.id;
        }
        finController.getFinanceDetail().then((response: String)=> {
            res.set("Content-Type","application/json; charset=utf-8");
            res.status(201);
            res.send(response);
        }).catch((error: any) => {
            res.set("Content-Type","application/json; charset=utf-8");
            res.status(500);
            res.send(error);
        }); 
    }
    
});

/**
 * Routes Definition for health, readiness and liveness check
 */
 // Route for liveness prone
 // The kubelet kills the container and restarts it.
router.get('/healthz',(_: any,res: any)=> {
    res.set("Content-Type","application/json; charset=utf-8");
    res.status(200);    
    res.send();
});


// Route for rediness check. 
// This route will return 200 in-case all required bootstarap is finished
// Note: We want to suspend traffic in-case there is something wrong here
router.get('/readiness',(_: any,res: any)=> {
    res.set("Content-Type","application/json; charset=utf-8");
    res.status(200);
    res.send();
});


// Protect slow starting containers with startup probes
router.get('/startup',(_: any,res: any)=> {
    res.set("Content-Type","application/json; charset=utf-8");
    res.status(200);
    res.send();
});

export default router;