import {PostRespBusinessObjects}    from "../mapping/bussObj/Response";
import FinanceController            from "../mapping/FinancesConroller";
import { json } from "express";

describe("Finance Controller test cases", ()=>{
    test("Finance controller initiated", () => {
        let financeController:FinanceController = new FinanceController();
        expect(financeController).toBeDefined();        
    });

    test("Setting mandatory properties", () => {
        let financeController:FinanceController = new FinanceController();
        financeController.setCustomerID = "123";
        financeController.setFinanceID = "456";
        expect(financeController).toBeDefined();        
    });

    test("Simulate Finance Fetching logic using Customer ID", () => {
        let financeController:FinanceController = new FinanceController();
        financeController.setCustomerID = "1234567";
        financeController.getFinanceDetail().then((response:String) => {
            let jsonResp:any = JSON.parse(response.toString());
            expect(jsonResp).toBeDefined();
            expect(jsonResp.metadata.status).toBe("0000");
            expect(jsonResp.finances.length>0).toBeTruthy();
        })
    });

    test("Simulate Finance Fetching logic using Finnance ID", () => {
        let financeController:FinanceController = new FinanceController();
        financeController.setFinanceID = "LD1715253561";
        financeController.getFinanceDetail().then((response:String) => {
            let jsonResp:any = JSON.parse(response.toString());
            expect(jsonResp).toBeDefined();
            expect(jsonResp.metadata.status).toBe("0000");
            expect(jsonResp.finances.length>0).toBeTruthy();
        })
    });

    test("Simulate Invalid Input scenario", () => {
        let financeController:FinanceController = new FinanceController();        
        financeController.getFinanceDetail().then((response:String) => {
            fail("Not expecting sucessfull response as required input is not set "+ response);
        }).catch((error:any)=> {
            let jsonResp:any = JSON.parse(error.toString());
            expect(jsonResp.metadata.status).toBe("9500");
        });
    });
    
});