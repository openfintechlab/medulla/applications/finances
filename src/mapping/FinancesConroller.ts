import financeJSON                      from "./simulator/finances.json"
import {PostRespBusinessObjects}        from "./bussObj/Response";
import FinanceResponseBO,{FinancesBO}   from "./bussObj/Finances";


export default class FinanceController{
    private _financeID!: string;
    private _customerID!: string;

    set setFinanceID(financeID: string){
        this._financeID = financeID;
    }

    set setCustomerID(customerID: string){
        this._customerID = customerID;
    }

    public getFinanceDetail(): Promise<String>{
        return new Promise<String>((resolve: any, reject: any) => {
            let finacesResponses:Array<FinancesBO> = new Array<FinancesBO>();
            let financeResp:FinancesBO;
            let objCount:number = 0;

            if(this._financeID === undefined && this._customerID === undefined){
                reject(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("9500","Invalid Request"));
            }            
            for(let index in financeJSON.finances){                
                if(this._customerID === financeJSON.finances[index].cif || 
                    this._financeID === financeJSON.finances[index].id){
                        let metadata:PostRespBusinessObjects.Metadata = new PostRespBusinessObjects.Metadata();
                        financeResp = new FinancesBO(
                            financeJSON.finances[index].id,
                            financeJSON.finances[index].type,                  
                            financeJSON.finances[index].desc,                  
                            financeJSON.finances[index].loanAmount,            
                            financeJSON.finances[index].loanCCY,               
                            financeJSON.finances[index].loanAmountLCY,         
                            financeJSON.finances[index].outstandingAmt,        
                            new Date(financeJSON.finances[index].startDate),             
                            new Date(financeJSON.finances[index].maturityDate),          
                            financeJSON.finances[index].tenor,                 
                            financeJSON.finances[index].drawDownAccountNumber, 
                            financeJSON.finances[index].repayFrequency,        
                            new Date(financeJSON.finances[index].lastPaymentDate),       
                            new Date(financeJSON.finances[index].nextRepaydate),         
                            financeJSON.finances[index].nextRepayAmount,       
                            financeJSON.finances[index].accountCategory       

                        );
                        objCount++;
                        finacesResponses.push(financeResp);                        
                }
            }
            if(objCount > 0){
                let metadata:PostRespBusinessObjects.Metadata = new PostRespBusinessObjects.Metadata();
                metadata.status = "0000";
                metadata.description = "Success!";
                metadata.responseTime = new Date();
                let finRespBO:FinanceResponseBO = new FinanceResponseBO(metadata, finacesResponses);                                
                resolve(finRespBO.generateJSON());
            }
            reject(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8404","Record not found"));         
        });
    }

    
}