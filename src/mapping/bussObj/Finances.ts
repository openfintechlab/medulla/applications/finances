import {PostRespBusinessObjects} from "./Response";

interface Finance {
    id:                    string;
    type:                  number;
    desc:                  string;
    loanAmount:            number;
    loanCCY:               string;
    loanAmountLCY:         number;
    outstandingAmt:        number;
    startDate:             Date;
    maturityDate:          Date;
    tenor:                 number;
    drawDownAccountNumber: string;
    repayFrequency:        string;
    lastPaymentDate:       Date;
    nextRepaydate:         Date;
    nextRepayAmount:       number;
    accountCategory:       string;
}

export class FinancesBO implements Finance{
    id:                    string;
    type:                  number;
    desc:                  string;
    loanAmount:            number;
    loanCCY:               string;
    loanAmountLCY:         number;
    outstandingAmt:        number;
    startDate:             Date;
    maturityDate:          Date;
    tenor:                 number;
    drawDownAccountNumber: string;
    repayFrequency:        string;
    lastPaymentDate:       Date;
    nextRepaydate:         Date;
    nextRepayAmount:       number;
    accountCategory:       string;

    constructor(
        id:                    string,
        type:                  number,
        desc:                  string,
        loanAmount:            number,
        loanCCY:               string,
        loanAmountLCY:         number,
        outstandingAmt:        number,
        startDate:             Date,
        maturityDate:          Date,
        tenor:                 number,
        drawDownAccountNumber: string,
        repayFrequency:        string,
        lastPaymentDate:       Date,
        nextRepaydate:         Date,
        nextRepayAmount:       number,
        accountCategory:       string
    ){
        this.id                    	=	id                    	;
        this.type                  	=	type                  	;
        this.desc                  	=	desc                  	;
        this.loanAmount            	=	loanAmount            	;
        this.loanCCY               	=	loanCCY               	;
        this.loanAmountLCY         	=	loanAmountLCY         	;
        this.outstandingAmt        	=	outstandingAmt        	;
        this.startDate             	=	startDate             	;
        this.maturityDate          	=	maturityDate          	;
        this.tenor                 	=	tenor                 	;
        this.drawDownAccountNumber 	=	drawDownAccountNumber 	;
        this.repayFrequency        	=	repayFrequency        	;
        this.lastPaymentDate       	=	lastPaymentDate       	;
        this.nextRepaydate         	=	nextRepaydate         	;
        this.nextRepayAmount       	=	nextRepayAmount       	;
        this.accountCategory       	=	accountCategory       	;
        

    }    
    
}



export default class FinanceResponseBO{
    metadata: PostRespBusinessObjects.Metadata;
    finances!: FinancesBO[];

    constructor(metadata:PostRespBusinessObjects.Metadata, finances:FinancesBO[]){
        this.metadata = metadata;        
        this.finances = finances;
    }

    public addFinance(finance: FinancesBO){
        this.finances.push(finance);
    }

    public generateJSON(): String{
        return JSON.stringify(this);
    }
}